<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Helper\Helper;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class AuthController extends Controller
{
    //
    /**
     * Register api
     *
     * @return \Illuminate\Http\Response
     */
    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|email|unique:users,email',
            'phone_number' => 'required|digits:10|unique:users,phone_number',
            'password' => 'required',
            'c_password' => 'required|same:password',
        ]);
   
        if($validator->fails()){
            return Helper::sendError(config('constants.validation_error_msg'),$validator->errors(), 200);       
        }
   
        $input = $request->all();
        $input['password'] = Hash::make($input['password']);
        $user = User::create($input);
        $success['token'] =  $user->createToken('UserToken')->accessToken;
        $success['name'] =  $user->name;
   
        return Helper::sendResponse('User register successfully.',$success);
    }
   
    /**
     * Login api
     *
     * @return \Illuminate\Http\Response
     */
    public function login(Request $request)
    {
        if(Auth::attempt(['phone_number' => $request->phone_number, 'password' => $request->password])){ 
            $user = Auth::user(); 
            $success['token'] =  $user->createToken('UserToken')->accessToken; 
            $success['name'] =  $user->name;
   
            return Helper::sendResponse(config('constants.user_auth_success_msg'),$success);
        } 
        else{ 
            return Helper::sendError(config('constants.user_auth_error_msg'));
        } 
    }

    //logout api
    public function logout(Request $request)
    {  
	        $result = $request->user()->token()->revoke();                  
            if($result){
                    $response = Helper::sendResponse('User logged out successfully.',[]);
              }else{
                    $response = Helper::sendError('Something went wrong.',[], 400);            
              }   
            return $response;       
    }
}
