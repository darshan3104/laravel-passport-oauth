<?php

namespace App\Helper;

use Carbon\Carbon;
use Illuminate\Support\Facades\Config;


class Helper
{

    /**
     * success response method.
     *
     * @return \Illuminate\Http\Response
     */
    public static function sendResponse($message, $result)
    {

        $response = [
            'success' => true,
            'message' => $message,
            'result'  => $result,
            'serverdatetime' => Carbon::now(),
            'db_version' => config('constants.db_version'),
            'api_version' => config('constants.api_version'),
            'http_code' => 200
        ];

        return response()->json($response, 200);
    }

    /**
     * return error response.
     *
     * @return \Illuminate\Http\Response
     */
    public static function sendError($message, $errorMessages = [], $code = 403)
    {
        //Response message array
        $response = [
            'success' => false,
            'message' => $message,
            'result' => $errorMessages,
            'http_code' => $code
        ];

        return response()->json($response, 200);
    }

}