<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Constant Variables
    |--------------------------------------------------------------------------
    |
    | This value is the name of your constant variable. This value is used when the
    | framework needs to place the application's constant name in a notification or
    | any other location as required by the application or its packages.
    |
    */

    //Latest app version details
    'api_version' => '1.0',
    'db_version' => '1.0',

    'validation_error_msg' => 'One or more parameters are missing or invalid',

    //User response messages

    'user_auth_middleware_error_msg' => 'Unauthorized user access',

    'user_auth_success_msg' => 'User is successfully logged in',
    'user_auth_error_msg' => 'This email id is not registered',
    'user_auth_otp_error_msg' => 'The OTP you entered is incorrect. Please enter a valid OTP',
    
    'list_user_profile_error_msg' => 'No profile details found for the user',
    'list_user_profile_success_msg' => 'Profile details are successfully listed',

    'user_update_profile_success_msg' => 'User details updated successfully',
    'user_update_profile_error_msg' => 'Error updating user details',

    'user_register_success_msg' => 'User is successfully registered',
    'user_register_error_msg' => 'Error while registering user',

];

?>